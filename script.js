console.log("FIRST SCRIPT");

let min = Math.floor(Math.random() * 60);
console.log("time, minute: " + min);

if (min <= 15) {
    console.log("first quarter");
} else if (min > 15 && min <= 30) {
    console.log("second quarter");
} else if (min > 30 && min <= 45) {
    console.log("third quarter");
} else {
    console.log("fifth quarter");
}
;

console.log("SECOND SCRIPT");
let x =["ua","en"];
let lang = x[Math.floor(Math.random()*2)];
console.log(`${lang}`);
const dayUa = ["Дні тижня Українською:", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота", "Неділя"];
const dayEn = ["Day of week in english:", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
switch (lang) {
    case "ua":
        console.log(dayUa);
        break;
    case "en":
        console.log(dayEn);
        break;
}
;
console.log("THIRD SCRIPT");

let str =Math.floor(Math.random() * 10000000000000);
console.log("Random number: "+str);

const StrArray = Array.from(String(str),Number);
console.log("Transfer in array: "+StrArray);
let SumFirstThree = StrArray[0]+StrArray[1]+StrArray[2]
console.log("Sum of first tree number: "+SumFirstThree);

const RevArray = StrArray.reverse();
console.log("Rever Array: "+RevArray);
let SumLastThree = RevArray[0]+RevArray[1]+RevArray[2]
console.log("Sum of last tree number: "+SumLastThree);

if (SumFirstThree===SumLastThree){
    console.log("You are luck");
}
else {
    console.log("Try again");
};